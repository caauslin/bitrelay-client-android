package me.caaus.bitrelay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class SmsRelay extends BroadcastReceiver {

    private static final String TAG = SmsRelay.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                String messageBody = "";

                SmsMessage[] messages = new SmsMessage[pdusObj.length];
                for (int i = 0; i < messages.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    messageBody += messages[i].getMessageBody();
                }

                // messages[0].getTimestampMillis() returns the sent time from service provider, which might not be suitable
                long timestamp = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
                String messageSender = messages[0].getOriginatingAddress();

                Log.d(TAG, "Received new message from " + messageSender + ": " + messageBody);

                try {
                    Relay(context, timestamp, messageSender, messageBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void Relay(Context context, final long timestamp, final String sender, final String content) throws JSONException {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_preferences_name), Context.MODE_PRIVATE);
        String serverUrl = sharedPreferences.getString(context.getString(R.string.pref_server_url), "") +
                context.getString(R.string.endpoint_sms);

        Log.d(TAG, "Posting data to: " + serverUrl);

        JSONObject postData = new JSONObject();
        postData.put("timestamp", timestamp);
        postData.put("sender", sender);
        postData.put("content", content);

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, serverUrl, postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "SMS relay succeed: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "SMS relay failed: " + error.toString());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");

                return headers;
            }
        };

        queue.add(request);
    }
}
