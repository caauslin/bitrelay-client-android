package me.caaus.bitrelay;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();

        final SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.shared_preferences_name), Context.MODE_PRIVATE);

        final EditText serverUrlText = findViewById(R.id.text_server_url);
        final Button saveButton = findViewById(R.id.btn_save);
        final Button testButton = findViewById(R.id.btn_test);
        final Button switchButton = findViewById(R.id.btn_switch);

        serverUrlText.setText(sharedPreferences.getString(getString(R.string.pref_server_url), ""));

        testButton.setEnabled(false);
        switchButton.setEnabled(false);

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = serverUrlText.getText().toString();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getString(R.string.pref_server_url), url);
                editor.apply();

                Toast.makeText(MainActivity.this, "Saving server URL ...", Toast.LENGTH_SHORT).show();
            }
        });

        testButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendTestSms();
            }
        });

        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO
                throw new UnsupportedOperationException();
            }
        });
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.RECEIVE_SMS }, 0);
        }
    }

    private void sendTestSms() {
        // TODO
        throw new UnsupportedOperationException();
    }
}
